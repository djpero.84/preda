# TEST FINALNI 


### ZADATAK

* napravite novi folder "final" i klonirajte projekat pomocu GIT komande unutar njega sa 
adrese: https://gitlab.com/djpero.84/preda.git
* napravite novi branch: fix-%vase_ime%
* inicijalizirajte projekat sa npm komandom
* instalirajte gulp, gulp-sass i browser-sync sa npm komandom
* napravite .gitignore fajl sa sadrzajem: node_modules (kako nebi komitovali taj folder)
* napravite sleducu strukturu fajlova:

```
scss/
css/
css/dist
javascript/
javascript/index.js
index.html
gulpfile.js
.gitignore
```

* iskopirajte gulpfile.js sa adrese: http://www.as-prijedor.com/gulpfile.js
* podesite gulpfile.js svojim potrebama tako da dobijete index.css koji morate da povezete
sa vasim index.html fajlom,
* u index.html povezite AngularJS (pretrazite angular.js CDN na google da dobijete adresu)
* stavite naslov stranice da bude "Finalni test"
* podesite vas html da radi sa Angularom
* napravite input polje u HTML-u 
* napravite h5 element, velicine 32px, da bude bold i centriran; koji ce prikazivati trenutnu vrijednost input polja i dok se kuca vrijednost u polju (two way binding)
* napravite $scope varijablu "zadatak" unutar vaseg index.js te vrijednost te varijable prikazite unutar p elementa koji ce biti ispod ova dva prethodna,
* napravite background sa bilo kojom slikom sa interneta preko citave stranice
* napravite link sa nazivom "AMAZON" i povezite tako da otvori stranicu: "www.amazon.com"
* h5 element i p element moraju imati crni background i zutu boju slova i centrirani
* komitujte kod i ukucajte poruku: "Ovo je finalni zadatak"
* push-ajte kod na server kada ste gotovi
